## Adding Tests.

We should really develop some tests for the app and only deploy if those tests pass. We will use the [Cypress][cy] testing framework - already installed from `package.json`.

Start the Cypress interactive test runner:

    $ npx cypress open

As this is the first run it creates a folder structure for your test code - see `cicdLab/cypress`. In `cicdLab/cypress/integration/examples` their are lots of coding examples for reference purposes. Remove this folder (or make a copy if you wish to learn more about Cypress later). Stop the test runner.

Create `cicdLab/cypress/integration/contacts-add.spec.js` and paste in this code:

    describe("Contact Add", () => {
      beforeEach(() => {
        cy.visit("/");
      });

      it("allows a contact to be added", () => {
        cy.get("span.badge").contains("5");
        cy.get("input[placeholder=Name]").type("user X");
        cy.get("input[placeholder=Address]").type("22 main street");
        cy.get('input[placeholder="Phone No."]').type("055 123456");
        cy.get("button")
          .contains("Add Contact")
          .click();
        cy.get("span.badge").contains("6");
      });
    });


Cypress uses `cypress.json` for configuration. Add the following entry to it:

    {
        "baseUrl": "http://localhost:8080/"
    }


In the test code we have the command:

>cy.visit("/")

Our configuration causes this command to visit: http://localhost:8080/. We must ensure the app is accessible from this URL before running the test. Start the web server:

        $ cd build
        $ httpserver -p 8080

In a second terminal start the test runner (from the base folder):

     $ npx cypress open

Click the test name on the left to run it. 

![][add]

Assuming the test result is 'green' (success), we should now get our CI build to include testing. However, Travis must use the headless test runner instead of the interactive one. Replace the content of `.travis.yml` with the following:


    dist: trusty
    language: node_js
    node_js:
    - "stable"
    notifications:
        email:
            recipients:
            - one@example.com      # change to your own address
            - other@example.com    # use other (optional)
            on_success: never        # default: change
            on_failure: always  
    cache:
      directories:
      - node_modules
    before_script:
    - npm install -g httpserver
    - npm run build
    - cd build && httpserver -p 8080 &
    script:
    - npx cypress run
    deploy:
      provider: surge
      skip_cleanup: true
      domain: mycontacts.surge.sh
      project: ./build
      on:
        branch: cypress-oldUI

[You must change the relevant lines according to your profile.]

In the 'before_script' section we install httpserver, build the app and start the server in background mode (&). In the 'script' section we simply execute the tests. Only if the tests are successful will the deployment take place.

Push this to GitHub:

    $ git add -A
    $ git commit -m "Include E2E testing in build process"
    $ git push origin master

On Travis, examine the log from the VM and notice the tests are executed and deployment happens. As an experiment, modify `cypress/integration/contacts-add.spec.js` to force a test fail. For example, change the last line to :

>>cy.get("span.badge").contains("10");

Push this to GitHub:

    $ git add -A
    $ git commit -m "Experiment: Failing test"
    $ git push origin master

This time the build fails and no deployment occurs.

UNDO THE TEST CODE CHANGE and push to Github again.


[add]: ./img/add.png
[cy]: https://www.cypress.io/