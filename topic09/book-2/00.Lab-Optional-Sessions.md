# User Authentication using methods

This lab improves the Hacker News API to demonstrate using methods in Mongoose to create a simple authentication middleware function.

You will update the Hacker News API to include the following:

- A User Model using static and instance methods.
- A ``/api/users`` route to allow users to authenticate.
- Express-Sessions package to manage/track user session
- Authentication middleware to block access to the ``/api/posts`` for unauthenticated users.
- Update the Hacker News model to use referencing.

## Initial set up

This exercise assumes you have finished the last lab. Make sure to commit your last changes to git before proceeding.

+ Install [express-session](https://www.npmjs.com/package/express-session) as a development dependency into the root of the Express application:

```bash
npm install --save express-session
```