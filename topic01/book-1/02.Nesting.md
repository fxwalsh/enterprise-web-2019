## Nested Objects.

Before attempting the exercise below, first examine the file `04_1_nested_objects.js` from the samples. Run it and ensure you understand how it generated its output.

In `fundamentals.js` the car's *type* property is a simple string but structurally it can be broken down into three parts: Make (eg Toyota), Model (eg Corolla), and CC (eg 1.8). __You are required__ to change this property of *aCar* from a string to an object with three properties, namely *make*, *model* and *cc*. To prove your change worked, add the following log statement to the bottom of the file:

    console.log(aCar.owner + ' drives a ' + aCar.type.make);

and run it as before: 

	$ node fundamentals.js 

It should display: __Joe Bloggs drives a Toyota__.

(The solution to this exercise is available [here][solution] under 'Solution 01'.)

Similarly, a car's *registration* is also composed of three distinct elements; year (10 for 2010), county code (WD for Waterford), and number (1058). __You are required__ to again change aCar to reflect this nested structure. Also add a log statement that displays the following: __Reg. = 10-WD-1058__.

(The solution is [here][solution] under 'Solution 02'.)

Before continuing, update the git repository, as follows:
 
        $ git add -A
        $ git commit -m "Nested objects"

[solution]: ./index.html#/Solutions