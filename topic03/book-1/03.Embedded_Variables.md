## Exercise 2 (Embedded variables).

In exercise 1 the course data was hard-coded in the JSX - course name, module details (name, no. of lectures, etc).  A slight improvement would be to place the data in local variables (say, a two-element array for the module details) and reference them from the JSX code as embedded variables. __You are required__ to implement this improvement, however, for convenience we will use a separate file/component. 

The steps to completing this exercise are as follows:

+ __Step 1__: In the browser, click the link: 'Samples -> 02 - JSX embedded variables'. Examine the related source code code: `components/samples/02_embeddedVariables.js`.
+ __Step 2__: Open the file  `components/exercises/02_embeddedVars.js` and copy over the *render()* method from the solution to exercise 1. Now adapt the code according to the specification given above. Assume the component is still 'static' in that their is exactly two modules in the course - no need for a loop construct. 
+ __Step 3__: In the browser, click the link: 'Exercises -> 02 - JSX embedded variables' to see the result. As before, do not change the class name as this will effect the rendering.

Hint: The pseudo-HTML code below is a guide to the solution required:
       
       . . . . . . 
       <tr>
          <td>modules[1].name</td>
          <td>modules[1].noLectures</td>
          <td>modules[1].noPracticals</td>
        </tr>
        . . . . . . . 

(See Exercise 2 solution [here][solutions].)

On completing the exercise, stop the server and type these commands:

        $ git add -A
        $ git commit -m "Exercise 2 completed"

Restart the server.

[solutions]: ./index.html#/Solutions
