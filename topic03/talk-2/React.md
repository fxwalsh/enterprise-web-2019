# Introduction to React and Storybook.
 
Examine the basics of React's component model, including declaration, JSX, props, composition, iteration. Introduce the Storybook development tool.