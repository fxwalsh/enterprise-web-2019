# Testing the Contacts API

### Create your first integration test

+ To enable us to test the app, we need to export the app object from *index.js* in the base folder of the project. Modify the *index.js* as follows:

```javascript
...
export const app = express(); //replaces the previous const app = express();
...
```

+ Create  new folder called *test* in the root folder of the lab.

+ As before, create a new eslint config file called *.eslintrc* file in *tests* with the following content:

~~~json
{
    "env": {
        "mocha": true
    },
    "rules": {
       "no-unused-vars":"off"
    }
}
~~~

+Create a new file called ``/test/testContactsApi.js`` with the following content.

```javascript
import supertest from 'supertest';
import {app} from '../index.js';
import should from 'should';

// UNIT test begin
describe('Contacts API test', function () {
    this.timeout(120000);
    // test #1: return a collection of json documents
    it('should return collection of JSON documents', function (done) {
        supertest(app)
            .get('/api/contacts')
            .expect('Content-type', /json/)
            .expect(200) // This is the HTTP response
            .then(res => {
                // HTTP status should be 200
                res.should.have.property('status').equal(200);
                done();
            });
    });
});
```

## Update package.json

+ Replace the *scripts* entry in the *package.json* file with the following.
```json

"scripts": {
  ....
  ,
   "test": "cross-env NODE_ENV=test mocha --require babel-core/register --require babel-polyfill  --exit",
   "pretest": "eslint *.js ./api/contacts/*.js ./test/*.js"
 }
```

The above script entry for test will set ``NODE_ENV`` to test and then run mocha against the files matching the pattern provided (i.e. by default it'll pick up the test in the test folder). You also need to update the start script to set ``NODE_ENV`` to 'development' before starting the server.
> **The pretest script is optional**. If you are linting, it might be a good idea to include a "full lint" before each test. When you run the test script, the pretest will be run automatically. 
Also, to ignore any linting errors relating to undeclared mocha functions like describe, create a new file called *.eslintrc* in the test folder and add the following content:

~~~json
{
    "env": {
        "mocha": true
    },
    "rules": {
       "no-unused-vars":"off"
    }
}
~~~

+ Now test by running the test script:

```bash
npm run test
```

The first time you run it you will probably get a few errors and warnings similar to what's shown in the following diagram:

![PreTest Linting Failures](./img/error1.png)

Fix the errors/warnings listed and you should see something similar to the following:

![First Successful Mocha Test](./img/main.png)

## Include Mockgoose

The current test is an integration test as it requires a real, functioning database. If we want our tests to just apply to the service interface then we should remove the dependency on the database. 

In our current solution, we connect to the database by importing the *db.js* module. Update the *db.js* module to use Mockgoose for testing as follows:

+ In *db.js*, Import the Mockgoose package

~~~javascript
import {Mockgoose} from 'mockgoose';
...
~~~

+ In *db.js*, replace the ``mongoose.connect(process.env.mongoDB);`` statement with the following code:

 ~~~ javascript
 ...
// Connect to database
if (process.env.NODE_ENV === 'test') {
    // use mockgoose for testing
    const mockgoose=new Mockgoose(mongoose);
    mockgoose.prepareStorage().then(()=>{
      mongoose.connect(process.env.mongoDB);
    });
  } else {
    // use the real deal for everything else
    mongoose.connect(process.env.mongoDB);
  }
...
~~~

This code will wrap the existing Mongoose object with Mockgoose only if the ``NODE_ENV`` environment variable is set to test (i.e. we're running test cases).

+ Now test again by running the test script:

```bash
npm run test
```

This time the first test run  will take longer as Mockgoose will create a local Mongodb in memory for the test. Subsequent tests will be faster though.
![First Mocha Test](./img/main.png)

## More api tests

Now lets try to test the add contact function of the API.

### Add a contact

+ Add another unit test to the last test. This time we'll use SuperTest to post a new contact and test for the correct status:
```javascript
// test #2 add a contact
    it('should add a contact', function (done) {
        // post to /api/contacts
        supertest(app)
            .post('/api/contacts')
            .send({
                name: 'Contact 99',
                address: '123 Strand St',
                age:23
            })
            .expect('Content-type', /json/)
            .expect(201)
            .then ((res) => {
                res.status.should.equal(201);
                res.body.should.have.property('_id');
                res.body.name.should.equal('Contact 99');
                done();
            });
    });
```

Now run the test again ``npm run test``. You should see something similar to the following:
![Add a contact test](./img/add_contact.png)

### Delete a Contact

For this test, you will delete the first contact in the list returned from the API:

+ Enter the following code:

```javascript
 // #3 delete a contact
    it('should delete a contact', () => {
      return  supertest(app)
            .get('/api/contacts')
            .expect('Content-type', /json/)
            .expect(200).then( (res) => {
               const id=res.body[0]._id;
               return supertest(app).delete(`/api/contacts/${id}`).expect(204); 
            }).then( (res) => {
                res.status.should.equal(204);  
            });
    });
```

## Challenge
Develop a test for the following route in the Contacts API
> PUT /api/contacts/[:id]

Use the notes and online resources for support.
